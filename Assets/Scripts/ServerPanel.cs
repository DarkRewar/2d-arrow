﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Arrows
{
    public class ServerPanel : MonoBehaviour
    {
        public InputField ServerAddressInput; 

        // Use this for initialization
        void Start()
        {
            NetworkManager.singleton.networkPort = 7777;
            ServerAddressInput.text = PlayerPrefs.GetString("ServerAddress", "localhost");
        }

        public void OnHostClicked()
        {
            NetworkManager.singleton.StartHost();
        }


        public void OnClientClicked()
        {
            PlayerPrefs.SetString("ServerAddress", ServerAddressInput.text);
            NetworkManager.singleton.networkAddress = ServerAddressInput.text;
            NetworkManager.singleton.StartClient();
        }
    }
}
