﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Arrows
{
    public class GameManager : MonoBehaviour
    {
        [Header("Paramètres")]

        /// <summary>
        /// Le score pour gagner.
        /// </summary>
        public int ScoreMax = 5;

        /// <summary>
        /// Le délai (en secondes) avant que la partie ne se ferme.
        /// </summary>
        public int DelayBeforeClose = 5;

        [Header("HUD")]

        /// <summary>
        /// Barre de vie du joueur 1.
        /// </summary>
        public Slider Player1Lifebar;

        /// <summary>
        /// Barre de rechargement du joueur 1.
        /// </summary>
        public Slider Player1Recovery;

        /// <summary>
        /// Barre de vie du joueur 2.
        /// </summary>
        public Slider Player2Lifebar;

        /// <summary>
        /// Barre de rechargement du joueur 2.
        /// </summary>
        public Slider Player2Recovery;

        /// <summary>
        /// Le text de score
        /// </summary>
        public Text ScoreText;

        [Header("Fin de Partie")]

        /// <summary>
        /// Message de fin de partie.
        /// </summary>
        public Text EndGameText;

        /// <summary>
        /// Image de victoire.
        /// </summary>
        public Image VictoryImage;

        /// <summary>
        /// Image de défaite.
        /// </summary>
        public Image DefeatImage;

        /// <summary>
        /// Score du joueur 1.
        /// </summary>
        private int _score1;

        /// <summary>
        /// Score du joueur 2.
        /// </summary>
        private int _score2;

        /// <summary>
        /// La partie est-elle finie ?
        /// </summary>
        private bool _finished = false;
        public bool IsFinished
        {
            get { return _finished; }
        }

        /// <summary>
        /// Timer de fin de partie.
        /// </summary>
        private float _timer = 0f;

        /// <summary>
        /// Le numéro du joueur de la session.
        /// </summary>
        private int _playerId;

        #region Singleton

        public static GameManager Current;

        public void Awake()
        {
            Current = this;
        }

        #endregion

        private void Start()
        {
            DefeatImage.gameObject.SetActive(false);
            VictoryImage.gameObject.SetActive(false);
            EndGameText.gameObject.SetActive(false);
        }

        /// <summary>
        /// Initialise le GameManager, le prévenant qui est le joueur courant.
        /// </summary>
        /// <param name="playerId"></param>
        public void Init(int playerId)
        {
            _playerId = playerId;
        }

        /// <summary>
        /// Rafraichissement de l'UI.
        /// </summary>
        public void LateUpdate()
        {
            if (ScoreText != null)
            {
                ScoreText.text = string.Format("{0} - {1}", _score2, _score1);
            }
        }

        /// <summary>
        /// Initialise les points de vie d'un joueur.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="life"></param>
        public void SetLifePlayer(int player, float life)
        {
            if(player == 1)
            {
                if(life <= 0)
                {
                    ++_score2;
                }
                Player1Lifebar.value = life;
            }
            else if(player == 2)
            {
                if (life <= 0)
                {
                    ++_score1;
                }
                Player2Lifebar.value = life;
            }

            if (_score1 == ScoreMax || _score2 == ScoreMax)
            {
                EndGame();
            }
        }

        /// <summary>
        /// Indique à l'interface qu'un joueur a tiré.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="recoveryTime"></param>
        public void Shot(int player, float recoveryTime)
        {
            StartCoroutine(AlterRecovery(player, recoveryTime));
        }

        /// <summary>
        /// Modifie le temps de rechargement dans l'interface.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="recoveryTime"></param>
        /// <returns></returns>
        private IEnumerator AlterRecovery(int player, float recoveryTime)
        {
            float time = 0;
            Slider slider = player == 1 ? Player1Recovery : Player2Recovery;
            slider.minValue = 0;
            slider.maxValue = recoveryTime;
            while(time < recoveryTime)
            {
                slider.value = time;
                time += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            time = recoveryTime;
            slider.value = time;
        }

        /// <summary>
        /// Déclenchement de la fin de partie.
        /// </summary>
        private void EndGame()
        {
            if (!_finished)
            {
                _finished = true;
                StartCoroutine(DoEndGame());
            }
        }

        private IEnumerator DoEndGame()
        {
            Image endImage;

            if(_playerId == 1 && _score1 == ScoreMax || _playerId == 2 && _score2 == ScoreMax)
            {
                endImage = VictoryImage;
            }
            else
            {
                endImage = DefeatImage;
            }

            Vector3 baseScale = endImage.gameObject.transform.localScale;
            endImage.gameObject.SetActive(true);
            endImage.gameObject.transform.localScale = Vector3.zero;

            float animation = 0f;
            while(animation < 1f)
            {
                endImage.gameObject.transform.localScale = Vector3.Lerp(
                    Vector3.zero, 
                    baseScale, 
                    animation
                );
                animation += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            endImage.gameObject.transform.localScale = baseScale;
            EndGameText.gameObject.SetActive(true);

            for(int i = DelayBeforeClose; i >= 0; --i)
            {
                EndGameText.text = string.Format("Fin de la partie dans {0} secondes.", i);
                yield return new WaitForSeconds(1f);
            }
            
            NetworkManager.singleton.StopClient();

            if (NetworkServer.active)
            {
                NetworkManager.singleton.StopServer();
            }
        }
    }
}
