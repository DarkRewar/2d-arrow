﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Arrows
{
    public class PlayerNetwork : NetworkBehaviour
    {
        /// <summary>
        /// L'ID du joueur en jeu.
        /// </summary>
        public int PlayerId = 1;

        /// <summary>
        /// Hauteur de saut.
        /// </summary>
        public float HighJump = 1f;

        /// <summary>
        /// Type de saut (côté moteur physique).
        /// </summary>
        public ForceMode2D JumpMode;

        /// <summary>
        /// Vitesse d'attaque.
        /// </summary>
        public float AttackSpeed = 1f;

        /// <summary>
        /// Point de spawn de la flèche, quand elle est tirée.
        /// </summary>
        public Transform ArrowSpawnPosition;

        /// <summary>
        /// Préfab de la flèche.
        /// </summary>
        public GameObject Arrow;

        /// <summary>
        /// Le son du rechargement de l'arc.
        /// </summary>
        [Header("Sound Design")]
        public AudioClip BowReloadSound;

        /// <summary>
        /// Le son de l'arc qui se tend.
        /// </summary>
        public AudioClip BowTendSound;

        /// <summary>
        /// Le son de l'arc qui tire.
        /// </summary>
        public AudioClip BowShotSound;

        /// <summary>
        /// Est-ce que le joueur peut être touché ?
        /// </summary>
        public bool CanBeTouched
        {
            get { return _lifepoints > 0; }
        }
        
        /// <summary>
        /// Rigidbody du joueur.
        /// </summary>
        private Rigidbody2D _rigidbody;

        /// <summary>
        /// Renderer du joueur.
        /// </summary>
        private SpriteRenderer _renderer;

        /// <summary>
        /// Audio Source du joueur.
        /// </summary>
        private AudioSource _audioSource;
        
        /// <summary>
        /// Points de vie actuels du joueur.
        /// </summary>
        private float _lifepoints = 3;

        /// <summary>
        /// Temps de rechargement de l'attaque.
        /// </summary>
        private float _attackSpeedCoolDown = 0f;

        /// <summary>
        /// Est-ce que le joueur peut sauter ?
        /// </summary>
        private bool _canJump = true;

        /// <summary>
        /// Est-ce que le joueur est en train de viser ?
        /// </summary>
        private bool _isAiming = false;

        /// <summary>
        /// Valeur de visée, entre 0 et 1.
        /// Au plus cette valeur est proche de 1, au plus
        /// la flèche sera tirée horizontalement.
        /// </summary>
        private float _aimValue = 0f;

        /// <summary>
        /// La rotation de base de ArrowSpawnPosition.
        /// </summary>
        private Vector3 _spawnRotation;

        /// <summary>
        /// Initialise le player id.
        /// </summary>
        private void Awake()
        {
            PlayerId = FindObjectsOfType<PlayerNetwork>().Length;
        }
        
        /// <summary>
        /// Départ du script.
        /// </summary>
        void Start()
        {
            _spawnRotation = ArrowSpawnPosition.localRotation.eulerAngles;
            _renderer = GetComponent<SpriteRenderer>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _audioSource = GetComponent<AudioSource>();

            if (isLocalPlayer)
            {
                GameManager.Current.Init(PlayerId);
            }

            GameManager.Current.SetLifePlayer(PlayerId, _lifepoints);
        }
        
        /// <summary>
        /// Update à chaque frame.
        /// Actuellement, ne sert qu'à la gestion des inputs.
        /// </summary>
        void Update()
        {
            if (!isLocalPlayer || GameManager.Current.IsFinished)
            {
                return;
            }

            // Recouvrement du cooldown du tir.
            if(_attackSpeedCoolDown > 0)
            {
                _attackSpeedCoolDown -= Time.deltaTime;
            }

            // Saut du joueur.
            if (Input.GetButtonDown("Jump") && _canJump)
            {
                _canJump = false;
                _rigidbody.AddForce(Vector2.up * HighJump, JumpMode);
            }

            // Prépare le tir.
            if (Input.GetButtonDown("Fire1") && _attackSpeedCoolDown <= 0)
            {
                DisplayShoot(true);
                CmdPlaySound(2);
            }

            // Plus on reste appuyé, plus la flèche partira droite.
            if (_isAiming)
            {
                _aimValue += Time.deltaTime;
                _aimValue = Mathf.Clamp01(_aimValue);
                ArrowSpawnPosition.localRotation = Quaternion.Euler(new Vector3(
                    0, 
                    0, 
                    Mathf.Lerp(_spawnRotation.z, 0, _aimValue)
                ));
            }

            // Lance le tir une fois le bouton relaché.
            if (Input.GetButtonUp("Fire1") && _isAiming)
            {
                _attackSpeedCoolDown = 1 / AttackSpeed;
                CmdShot(ArrowSpawnPosition.position, ArrowSpawnPosition.rotation, _aimValue);
                _aimValue = 0f;
                DisplayShoot(false);
            }
        }

        /// <summary>
        /// Applique le feedback du recovery time de tir dans l'interface.
        /// </summary>
        void ApplyShotFeedback()
        {
            GameManager.Current.Shot(PlayerId, 1 / AttackSpeed);
        }

        /// <summary>
        /// Applique le feedback du recovery time de tir dans l'interface,
        /// en fonction d'une durée donnée.
        /// </summary>
        /// <param name="duration"></param>
        void ApplyShotFeedback(float duration)
        {
            GameManager.Current.Shot(PlayerId, duration);
        }

        /// <summary>
        /// Déclenche le tir sur le serveur.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="rot"></param>
        /// <param name="force"></param>
        [Command]
        void CmdShot(Vector3 pos, Quaternion rot, float force)
        {
            GameObject arrow = Instantiate(Arrow, pos, rot);
            arrow.GetComponent<Arrow>().Force += arrow.GetComponent<Arrow>().Force * force * 0.75f;
            NetworkServer.Spawn(arrow);
            RpcShot();

            // Sécurité : on supprime la flèche si jamais elle ne l'est pas après 5 secondes.
            Destroy(arrow, 5);
        }

        /// <summary>
        /// Applique le feedback de tir chez les clients.
        /// </summary>
        [ClientRpc]
        void RpcShot()
        {
            ApplyShotFeedback();
            PlaySound(3);
        }

        /// <summary>
        /// Modifie la vie du joueur et en informe le serveur.
        /// </summary>
        /// <param name="life"></param>
        public void AlterLife(float life)
        {
            CmdSetLife(_lifepoints + life);
        }

        /// <summary>
        /// Modifie la vie du joueur et en informe le client.
        /// </summary>
        /// <param name="life"></param>
        [Command]
        public void CmdSetLife(float life)
        {
            RpcSetLife(life);
        }

        /// <summary>
        /// Réception de la modification de la vie d'un joueur (sur le client).
        /// </summary>
        /// <param name="life"></param>
        [ClientRpc]
        public void RpcSetLife(float life)
        {
            SetLife(life);
        }

        /// <summary>
        /// Applique la modification de la vie d'un joueur dans l'interface et sur l'objet.
        /// </summary>
        /// <param name="life"></param>
        public void SetLife(float life)
        {
            _lifepoints = life;
            Debug.Log(string.Format("Player[{0}] = {1} / {2}", PlayerId, life, _lifepoints));
            GameManager.Current.SetLifePlayer(PlayerId, _lifepoints);
            
            if(life <= 0)
            {
                StartCoroutine(Respawn(3f));
            }
        }

        /// <summary>
        /// Quand le joueur entre en collision.
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag.Equals("Ground"))
            {
                _canJump = true;
            }
        }

        /// <summary>
        /// Demande à jouer le son sur les clients.
        /// </summary>
        /// <param name="sound"></param>
        [Command]
        private void CmdPlaySound(int sound)
        {
            RpcPlaySound(sound);
        }
        
        /// <summary>
        /// Les clients doivent jouer le son.
        /// </summary>
        /// <param name="sound"></param>
        [ClientRpc]
        private void RpcPlaySound(int sound)
        {
            PlaySound(sound);
        }

        /// <summary>
        /// Joue le son demandé.
        /// </summary>
        /// <param name="sound"></param>
        private void PlaySound(int sound)
        {
            _audioSource.Stop();

            if (sound == 1)
            {
                _audioSource.PlayOneShot(BowReloadSound);
            }
            else if (sound == 2)
            {
                _audioSource.PlayOneShot(BowTendSound);
            }
            else if (sound == 3)
            {
                _audioSource.PlayOneShot(BowShotSound);
            }
        }

        /// <summary>
        /// Affiche le signe de la visée du joueur (quand le joueur veut tirer).
        /// </summary>
        /// <param name="display"></param>
        private void DisplayShoot(bool display)
        {
            _isAiming = display;
            ArrowSpawnPosition.gameObject.SetActive(display);
            ArrowSpawnPosition.localRotation = Quaternion.Euler(_spawnRotation);
        }

        /// <summary>
        /// Réapparition du joueur.
        /// </summary>
        /// <param name="respawnTime"></param>
        /// <returns></returns>
        private IEnumerator Respawn(float respawnTime)
        {
            DisplayShoot(false);
            _attackSpeedCoolDown = respawnTime;
            ApplyShotFeedback(respawnTime);
            _renderer.color = new Color(1, 1, 1, 0.3f);
            yield return new WaitForSeconds(respawnTime);
            _renderer.color = new Color(1, 1, 1, 1);
            _lifepoints = 3f;
            GameManager.Current.SetLifePlayer(PlayerId, _lifepoints);
        }
    }
}
