﻿using Arrows;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Arrows
{
    public class Arrow : NetworkBehaviour
    {
        public float Force = 1f;
        public ForceMode2D ForceMode;
        public SpriteRenderer ArrowRenderer;
        public GameObject BloodSpreadPrefab;

        private BoxCollider2D _collider;
        private Rigidbody2D _rigidbody;
        private AudioSource _audioSource;

        // Use this for initialization
        void Start()
        {
            _collider = GetComponent<BoxCollider2D>();
            _audioSource = GetComponent<AudioSource>();
            _rigidbody = GetComponent<Rigidbody2D>();

            _rigidbody.AddForce(transform.right * Force, ForceMode);
            _rigidbody.AddTorque(0.025f);
        }

        /// <summary>
        /// Si la flèche entre en collision, côté serveur,
        /// elle est alors détruite.
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter2D(Collision2D collision)
        {
            _audioSource.Play();

            if (collision.gameObject.tag.Equals("Player"))
            {
                GameObject blood = Instantiate(
                    BloodSpreadPrefab, 
                    collision.contacts[0].point, 
                    Quaternion.Euler(new Vector2(0, collision.collider.transform.localEulerAngles.y))
                );
                Destroy(blood, 4);

                _collider.enabled = false;
                ArrowRenderer.enabled = false;

                if (!isServer)
                    return;

                PlayerNetwork pNet = collision.gameObject.GetComponent<PlayerNetwork>();
                if (pNet.CanBeTouched)
                {
                    pNet.AlterLife(-1);

                    Destroy(gameObject, 2);
                }
            }
        }
    }
}
